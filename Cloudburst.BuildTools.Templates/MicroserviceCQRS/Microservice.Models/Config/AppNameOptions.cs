using Cloudburst.Configuration.Attributes;

namespace Microservice.Models.Config {
	// Set this to the structure you would prefer
	// For
	// {
	//   "CompanyName": {
	//     "AppName": {
	//       "DatabaseConnection": "Server=localhost..."
	//     }
	//   }
	// }
	// use the attribute as [ConfigurationKey("CompanyName:AppName")]
	[ConfigurationKey("AppName")]
	public class AppNameOptions {
		public string DatabaseConnection { get; set; }
	}
}
