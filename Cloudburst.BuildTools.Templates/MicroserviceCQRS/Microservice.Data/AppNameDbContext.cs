using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Microservice.Data {
	public class AppNameDbContext : DbContext {

		// Add your entities here.
		//public DbSet<MyEntity> MyEntities { get; set; }

		public AppNameDbContext(DbContextOptions<AppNameDbContext> options) : base(options) { }

		protected override void OnModelCreating(ModelBuilder modelBuilder) {
			// Add your model configuration here
		}
	}
}
