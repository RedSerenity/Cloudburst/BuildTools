using Cloudburst.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Microservice.Data {
	public class AppNameDbContextFactory : IDesignTimeDbContextFactory<AppNameDbContext> {

		public readonly string ConnectionString = DesignTimeDbContextFactoryConfiguration.GetDesignTimeDbContextConnectionString();

		public AppNameDbContext CreateDbContext(string[] args) {
			var optionsBuilder = new DbContextOptionsBuilder<AppNameDbContext>();
#if (DataUseEfCoreMySql)
			optionsBuilder.UseMySql(ConnectionString);
#endif
			return new AppNameDbContext(optionsBuilder.Options);
		}

	}
}
