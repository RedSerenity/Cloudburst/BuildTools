﻿using AutoMapper;
using Cloudburst.Configuration;
using Cloudburst.HealthChecks;
using Cloudburst.ServiceRegistration;
using Cloudburst.Version;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microservice.Data;
using Microservice.Grpc.Controllers;
using Microservice.Models.Config;

namespace Microservice.Api {
	public class Startup {
		private readonly IConfiguration _configuration;

		public Startup(IConfiguration configuration) {
			_configuration = configuration;
		}

		public void ConfigureServices(IServiceCollection services) {
			var options = services.ConfigureMonitor<AppNameOptions>(_configuration);

#if (DataUseEfCoreMySql)
			services.AddDbContext<AppNameDbContext>(config => {
				config.UseMySql(options.CurrentValue.DatabaseConnection);
			});
#endif
			services.AddGrpc();
			services.AddMediatR(typeof(Cqrs.Identifier), typeof(Cqrs.Handlers.Identifier));
			services.AddAutoMapper(typeof(Mappings.Identifier));
		}

		public void Configure(IApplicationBuilder app, IWebHostEnvironment env) {
			if (env.IsDevelopment()) {
				app.UseDeveloperExceptionPage();
			}

			app.UseRouting();
			app.UseServiceRegistration();
			app.UseCloudburstVersioning();

			app.UseEndpoints(endpoints => {
				// Add your Grpc Services here
				endpoints.MapGrpcService<HelloWorldController>();

				endpoints.MapCloudburstHealthChecks();

				endpoints.MapGet("/", async context => { await context.Response.WriteAsync("Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909"); });
			});
		}
	}
}
