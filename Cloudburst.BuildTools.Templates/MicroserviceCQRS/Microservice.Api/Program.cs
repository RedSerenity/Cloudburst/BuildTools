using Cloudburst.Core;

namespace Microservice.Api {
	public class Program {
		// Randomly generated port number. Change as needed.
		private const int BASE_PORT = RandomPortNumber;

		public static void Main(string[] args) =>
			Cloudburst.Core.Cloudburst.Downpour<Startup, AppNameVersion>(args, webBuilder => {
				webBuilder.ConfigurePorts(BASE_PORT, BASE_PORT+1);
			});
	}
}
