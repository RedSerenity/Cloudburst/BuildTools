using System;

namespace Microservice.Cqrs.Responses {
	public class HelloWorldResponse {
		public string Hello { get; set; } = String.Empty;
	}
}
