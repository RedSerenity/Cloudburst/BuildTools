using MediatR;
using Microservice.Cqrs.Responses;

namespace Microservice.Cqrs {
	public class HelloWorldRequest : IRequest<HelloWorldResponse> {
		public string Name { get; set; }
	}
}
