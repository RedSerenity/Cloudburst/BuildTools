using AutoMapper;

namespace Microservice.Mappings {
	public class AppNameProfile : Profile {
		public AppNameProfile() {
			// Add your AutoMapper mappings here.
			// Create additional Profile Classes as needed...

			// Map GRPC Requests to CQRS Commands
			CreateMap<Grpc.HelloWorldRequest, Cqrs.HelloWorldRequest>();

			// Map CQRS Responses to GRPC Responses
			CreateMap<Cqrs.Responses.HelloWorldResponse, Grpc.HelloWorldResponse>();

			// Map CQRS Commands to POCO Models (EF Core/Dapper)
			//CreateMap<Cqrs.HelloWorldRequest, Models.Entities.MyEntity>();
		}
	}
}
