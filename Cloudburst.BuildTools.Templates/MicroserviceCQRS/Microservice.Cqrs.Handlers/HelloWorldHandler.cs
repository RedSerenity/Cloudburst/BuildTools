using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.Extensions.Logging;
using Microservice.Cqrs.Responses;

namespace Microservice.Cqrs.Handlers {
	public class HelloWorldHandler : IRequestHandler<HelloWorldRequest, HelloWorldResponse> {
		private readonly ILogger<HelloWorldHandler> _logger;
		private readonly IMapper _mapper;

		public HelloWorldHandler(ILogger<HelloWorldHandler> logger, IMapper mapper) {
			_logger = logger;
			_mapper = mapper;
		}

		public async Task<HelloWorldResponse> Handle(HelloWorldRequest request, CancellationToken cancellationToken) {
			return await Task.FromResult(new HelloWorldResponse {
				Hello = $"Hello, {request.Name}!"
			});
		}
	}
}
