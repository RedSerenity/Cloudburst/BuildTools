using System.Threading.Tasks;
using AutoMapper;
using Grpc.Core;
using MediatR;
using Microsoft.Extensions.Logging;
using Microservice.Cqrs;
using Microservice.Cqrs.Responses;

namespace Microservice.Grpc.Controllers {
	public class HelloWorldController : HelloWorldService.HelloWorldServiceBase {
		private readonly ILogger<HelloWorldController> _logger;
		private readonly IMediator _mediator;
		private readonly IMapper _mapper;

		public HelloWorldController(ILogger<HelloWorldController> logger, IMediator mediator, IMapper mapper) {
			_logger = logger;
			_mediator = mediator;
			_mapper = mapper;
		}

		public override async Task<HelloWorldResponse> HelloWorld(Grpc.HelloWorldRequest request, ServerCallContext context) {
			// Cqrs.HelloWorldRequest should be called HelloWorldCommand or HelloWorldQuery depending on the context.
			return await SendCommand<Grpc.HelloWorldRequest, Grpc.HelloWorldResponse, Cqrs.HelloWorldRequest>(request);
		}

		private async Task<TResult> SendCommand<TRequest, TResult, TCommandQuery>(TRequest request) {
			TCommandQuery command = _mapper.Map<TCommandQuery>(request);
			object response = await _mediator.Send(command);
			TResult result = _mapper.Map<TResult>(response);
			return result;
		}
	}
}
