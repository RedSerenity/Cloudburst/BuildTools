using System;
using Microsoft.Build.Framework;

namespace Cloudburst.BuildTools.Models {
	public class ProjectDetails {
		public string ProjectGroupName => "Cloudburst";

		private string _ns;
		[Required]
		public string Namespace {
			get => $"{ProjectGroupName}.{_ns}";
			set {
				if (String.IsNullOrEmpty(value)) {
					throw new ArgumentNullException(nameof(Namespace));
				}

				string pgn = $"{ProjectGroupName}.";
				if (value.StartsWith(pgn)) {
					_ns = value.Remove(value.IndexOf(pgn, StringComparison.Ordinal), pgn.Length);
				} else {
					_ns = value;
				}
			}
		}

		[Required]
		public string Title { get; set; }
		[Required]
		public string Description { get; set; }

		[Required]
		public string RepositoryName { get; set; }
		public string RepositoryUrl => $"{RepositoryBaseAddress}/{RepositoryName}";
		public string RepositoryBaseRawUrl => $"{RepositoryBaseAddress}/{RepositoryName}/-/raw/master";
		public string RepositoryBaseAddress => "https://gitlab.com/RedSerenity/Cloudburst";

		public string ProjectUrl => "https://getcloudburst.com";
		public string PackageProjectUrl => $"{ProjectUrl}/{_ns}";
		public string ProjectLicenseType => "MIT";
		public string ProjectLicenseFile => "LICENSE";
		public string ProjectIconFile => "icon.png";
		public string ProjectAuthor => "Tyler Andersen";
		public string ProjectCopyright => $"Copyright © {DateTime.Now.Year} {ProjectAuthor}";
	}
}
