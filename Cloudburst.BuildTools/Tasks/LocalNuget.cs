using System;
using System.IO;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;

namespace Cloudburst.BuildTools.Tasks {
	public class LocalNuget : Task {
		public string DirName { get; set; } = "LocalNugetPackages";
		public bool CreateMissing { get; set; } = true;
		[Output]
		public string LocalNugetPath { get; set; }

		public override bool Execute() {
			string currentDirectory = Directory.GetCurrentDirectory();
			string foundPath = FindLocalNuget(currentDirectory, DirName);

			if (String.IsNullOrEmpty(foundPath)) {
				if (CreateMissing) {
					foundPath = Path.GetFullPath($"{currentDirectory}/{DirName}");
					Directory.CreateDirectory(foundPath);
					Log.LogMessage($"Created non-existent directory {foundPath}");
				} else {
					Log.LogError($"Could not find {DirName}");
					return false;
				}
			}

			LocalNugetPath = foundPath;

			return true;
		}

		private string FindLocalNuget(string path, string dirName) {
			while (true) {
				string fullPath = Path.GetFullPath($"{path}/{dirName}");

				Log.LogMessage($"Looking for {fullPath}");

				if (Directory.Exists(fullPath)) {
					return fullPath;
				}

				DirectoryInfo parentDirectory = Directory.GetParent(path);
				if (parentDirectory is null) {
					return null;
				}
				path = parentDirectory.ToString();
			}
		}
	}
}
