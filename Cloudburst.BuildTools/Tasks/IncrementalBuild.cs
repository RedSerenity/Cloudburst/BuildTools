﻿using System;
using System.IO;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;

namespace Cloudburst.BuildTools.Tasks {
	public class IncrementalBuild : Task {
		private const string BUILD_VERSION_FILE = "Build.Version";

		[Output]
		public int Build { get; set; }

		public override bool Execute() {
			Build = ReadBuildNumber();
			IncrementAndSaveBuildNumber();

			Log.LogMessage(MessageImportance.High, $"Build Number: {Build}");
			return true;
		}

		private int ReadBuildNumber() {
			if (!File.Exists(BUILD_VERSION_FILE)) {
				return 999;
			}

			string file = File.ReadAllText(BUILD_VERSION_FILE);
			return Int32.Parse(file);
		}

		private int IncrementAndSaveBuildNumber() {
			Build++;
			WriteBuildNumber(Build);
			return Build;
		}

		private void WriteBuildNumber(int buildNumber) {
			File.WriteAllText(BUILD_VERSION_FILE, buildNumber.ToString());
		}
	}
}
