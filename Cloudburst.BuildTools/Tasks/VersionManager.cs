using System;
using System.IO;
using System.Text.RegularExpressions;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;

namespace Cloudburst.BuildTools.Tasks {
	public enum ConfigurationType {
		Debug,
		Release
	}

	public class VersionManager : Task {
		private const string VERSION_FILE = "Version";

		private string VersionFile => !String.IsNullOrEmpty(VersionFilePath) ? Path.GetFullPath($"{VersionFilePath}/{VERSION_FILE}") : VERSION_FILE;

		private ConfigurationType _configuration;

		public string Configuration {
			get => _configuration.ToString();
			set {
				try {
					Log.LogMessage(MessageImportance.High, $"Using Configuration: {value}");
					_configuration = (ConfigurationType) Enum.Parse(typeof(ConfigurationType), value);
				} catch (Exception) {
					_configuration = ConfigurationType.Debug;
				}
			}
		}

		private bool _dontIncrement = false;
		public string DontIncrement {
			get => _dontIncrement.ToString();
			set {
				try {
					_dontIncrement = Convert.ToBoolean(value);
				} catch (FormatException) {
					// An Int as a string will trigger this.
					var result = Int32.Parse(value);
					_dontIncrement = Convert.ToBoolean(result);
				} catch (Exception) {
					throw new Exception($"Cannot convert {value} to a boolean.");
				}
			}
		}

		public string VersionFilePath { get; set; }

		[Output]
		public string Version => $"{Major}.{Minor}.{Patch}.{Build}";

		[Output]
		public int Major { get; set; }

		[Output]
		public int Minor { get; set; }

		[Output]
		public int Patch { get; set; }

		[Output]
		public int Build { get; set; }

		public override bool Execute() {
			LoadVersion();
			SaveVersion();
			return true;
		}

		private void LoadVersion() {
			if (!File.Exists(VersionFile)) {
				Log.LogMessage(MessageImportance.High, $"Version file does not exist.");
				Major = 0;
				Minor = 1;
				Patch = 0;
				Build = 1000;
				SaveVersion();
				return;
			}

			string version = File.ReadAllText(VersionFile);

			Log.LogMessage(MessageImportance.High, $"Read Version file: {version}");

			string splitPattern = @"\.";
			Regex regex = new Regex(splitPattern);
			string[] splitVersion = regex.Split(version);

			switch (splitVersion.Length) {
				case 0:
					Major = 0;
					Minor = 1;
					Patch = 0;
					Build = 1000;
					break;
				case 1:
					Major = Int32.Parse(splitVersion[0]);
					Minor = 0;
					Patch = 0;
					Build = 1000;
					break;
				case 2:
					Major = Int32.Parse(splitVersion[0]);
					Minor = Int32.Parse(splitVersion[1]);
					Patch = 0;
					Build = 1000;
					break;
				case 3:
					Major = Int32.Parse(splitVersion[0]);
					Minor = Int32.Parse(splitVersion[1]);
					Patch = _configuration == ConfigurationType.Release && !_dontIncrement ?
						Int32.Parse(splitVersion[2]) + 1 :
						Int32.Parse(splitVersion[2]);
					Build = 1000;
					break;
				case 4:
					Major = Int32.Parse(splitVersion[0]);
					Minor = Int32.Parse(splitVersion[1]);
					Patch = _configuration == ConfigurationType.Release && !_dontIncrement ?
						Int32.Parse(splitVersion[2]) + 1 :
						Int32.Parse(splitVersion[2]);
					Build = _dontIncrement ?
						Int32.Parse(splitVersion[3]) :
						Int32.Parse(splitVersion[3]) + 1;
					break;
			}

			Log.LogMessage(MessageImportance.High, $"Final Version is: {Version}");
		}

		private void SaveVersion() {
			File.WriteAllText(VersionFile, Version);
		}
	}
}
