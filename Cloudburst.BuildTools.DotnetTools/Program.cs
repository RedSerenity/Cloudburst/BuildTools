﻿using System.Threading.Tasks;
using Cloudburst.BuildTools.DotnetTools.Git;
using Cloudburst.BuildTools.DotnetTools.Publish;
using Cloudburst.BuildTools.DotnetTools.Version;
using CommandLine;

namespace Cloudburst.BuildTools.DotnetTools {
	class Program {
		public static async Task<int> Main(string[] args) {
			return await Parser.Default.ParseArguments<PublishOptions, BumpVersionOptions, GetVersionOptions, SetVersionOptions, GitCommitOptions, GitPushOptions>(args)
				.MapResult(
					async (PublishOptions options) => await PublishCommand.RunPublishAsync(options),
					async (BumpVersionOptions options) => await VersionCommand.RunBumpVersionAsync(options),
					async (GetVersionOptions options) => await VersionCommand.RunGetVersionAsync(options),
					async (SetVersionOptions options) => await VersionCommand.RunSetVersionAsync(options),
					async (GitCommitOptions options) => await GitCommand.RunGitCommitAsync(options),
					async (GitPushOptions options) => await GitCommand.RunGitPushAsync(options),
					_ => Task.FromResult(1)
				);
		}
	}
}
