using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Cloudburst.BuildTools.DotnetTools.Version {
	public class VersionCommand {
		public static async Task<int> RunBumpVersionAsync(BumpVersionOptions options) {
			FindVersionFiles(
				options.VersionFile,
				(currentFile, directory) => {
					string version = GetVersion(currentFile);
					Console.WriteLine($"Current Version: {version}");

					var splitVersion = VersionByField(SplitVersion(version), options.RawVersionType, i => i + 1);

					Console.WriteLine($"New Version: {String.Join('.', splitVersion)}");
					SetVersion(currentFile, splitVersion);
				},
				(childFile, directory) => {
					string version = GetVersion(childFile);
					Console.WriteLine($"{directory}");
					Console.WriteLine($"\tCurrent: {version}");

					var splitVersion = VersionByField(SplitVersion(version), options.RawVersionType, i => i + 1);

					Console.WriteLine($"\tNew: {String.Join('.', splitVersion)}");
					SetVersion(childFile, splitVersion);
				}
			);

			return 0;
		}

		public static async Task<int> RunGetVersionAsync(GetVersionOptions options) {
			FindVersionFiles(
				options.VersionFile,
				(currentFile, directory) => {
					string version = GetVersion(currentFile);

					if (options.RawVersionType == VersionType.All) {
						Console.WriteLine(version);
						return;
					}

					VersionByField(SplitVersion(version), options.RawVersionType, i => {
						Console.WriteLine(i);
						return 0;
					});
				},
				(childFile, directory) => {
					string version = GetVersion(childFile);
					if (options.RawVersionType == VersionType.All) {
						Console.WriteLine($"{directory}: {version}");
						return;
					}

					VersionByField(SplitVersion(version), options.RawVersionType, i => {
						Console.WriteLine($"{directory}: {i}");
						return 0;
					});
				}
			);

			return 0;
		}

		public static async Task<int> RunSetVersionAsync(SetVersionOptions options) {
			FindVersionFiles(
				options.VersionFile,
				(currentFile, directory) => {
					string version = GetVersion(currentFile);
					int[] splitVersion;

					Console.WriteLine($"Current Version: {version}");

					if (options.RawVersionType != VersionType.All) {
						splitVersion = VersionByField(SplitVersion(version), options.RawVersionType, i => Int32.Parse(options.Version));
					} else {
						splitVersion = SplitVersion(options.Version);
					}

					Console.WriteLine($"New Version: {String.Join('.', splitVersion)}");
					SetVersion(currentFile, splitVersion);
				},
				(childFile, directory) => {
					string version = GetVersion(childFile);
					int[] splitVersion;

					Console.WriteLine($"{directory}");
					Console.WriteLine($"\tCurrent: {version}");

					if (options.RawVersionType != VersionType.All) {
						splitVersion = VersionByField(SplitVersion(version), options.RawVersionType, i => Int32.Parse(options.Version));
					} else {
						splitVersion = SplitVersion(options.Version);
					}

					Console.WriteLine($"\tNew: {String.Join('.', splitVersion)}");
					SetVersion(childFile, splitVersion);
				}
			);

			return 0;
		}

		private static string GetVersion(string versionFile) {
			return File.ReadAllText(versionFile) ?? "0.0.0.0";
		}

		private static void SetVersion(string versionFile, int[] version) {
			string fullVersion = String.Join('.', version);
			SetVersion(versionFile, fullVersion);
		}

		private static void SetVersion(string versionFile, string version) {
			File.WriteAllText(versionFile, version);
		}

		// Action<VersionFile, DirectoryName>, Action<VersionFile, DirectoryName>
		private static void FindVersionFiles(string versionFilename, Action<string, string> actionCurrent, Action<string, string> actionChild = null) {
			var versionFile = Path.GetFullPath($"{Directory.GetCurrentDirectory()}/{versionFilename}");

			if (File.Exists(versionFile)) {
				actionCurrent?.Invoke(versionFile, Path.GetFileName(Directory.GetCurrentDirectory()));
			}

			// Let's check the child directories now
			List<string> directories = Directory.EnumerateDirectories(Directory.GetCurrentDirectory()).ToList();

			foreach (string directory in directories) {
				versionFile = Path.GetFullPath($"{directory}/{versionFilename}");

				if (!File.Exists(versionFile)) continue;
				actionChild?.Invoke(versionFile, Path.GetFileName(directory));
			}
		}

		private static int[] SplitVersion(string version) {
			string splitPattern = @"\.";
			Regex regex = new Regex(splitPattern);
			string[] splitVersion = regex.Split(version);

			return splitVersion.Length switch {
				1 => new[] {Int32.Parse(splitVersion[0]), 1, 0, 1000},
				2 => new[] {Int32.Parse(splitVersion[0]), Int32.Parse(splitVersion[1]), 0, 1000},
				3 => new[] {Int32.Parse(splitVersion[0]), Int32.Parse(splitVersion[1]), Int32.Parse(splitVersion[2]), 1000},
				4 => new[] {Int32.Parse(splitVersion[0]), Int32.Parse(splitVersion[1]), Int32.Parse(splitVersion[2]), Int32.Parse(splitVersion[3])},
				_ => new[] {0, 1, 0, 1000}
			};
		}

		private static int[] VersionByField(int[] version, VersionType versionType, Func<int, int> modifyVersion) {
			switch (versionType) {
				case VersionType.Major:
					version[0] = modifyVersion(version[0]);
					break;
				case VersionType.Minor:
					version[1] = modifyVersion(version[1]);
					break;
				case VersionType.Patch:
					version[2] = modifyVersion(version[2]);
					break;
				case VersionType.Build:
					version[3] = modifyVersion(version[3]);
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(versionType), versionType, null);
			}

			return version;
		}

	}
}
