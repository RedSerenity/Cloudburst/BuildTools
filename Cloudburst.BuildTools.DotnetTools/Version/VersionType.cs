namespace Cloudburst.BuildTools.DotnetTools.Version {
	public enum VersionType {
		Major,
		Minor,
		Patch,
		Build,
		All
	}
}
