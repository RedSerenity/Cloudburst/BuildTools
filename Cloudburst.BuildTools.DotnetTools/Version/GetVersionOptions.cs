using System;
using CommandLine;

namespace Cloudburst.BuildTools.DotnetTools.Version {
	[Verb("get-version", HelpText = "Get the current version number")]
	public class GetVersionOptions {
		private VersionType _versionType;
		private string _versionFile;

		public VersionType RawVersionType => _versionType;

		[Option('f', "field", Required = true, HelpText = "Major, Minor, Patch, Build, All")]
		public string VersionType {
			get => _versionType.ToString();
			set {
				try {
					_versionType = Enum.Parse<VersionType>(value);
				} catch (Exception) {
					throw new Exception("Invalid option for --bump. Must be Major, Minor, Patch, Build, or All");
				}
			}
		}

		[Option('e', "versionFile", Required = false, HelpText = "Version file to modify. Defaults to 'Version'")]
		public string VersionFile {
			get => String.IsNullOrEmpty(_versionFile) ? "Version" : _versionFile;
			set => _versionFile = value;
		}
	}
}
