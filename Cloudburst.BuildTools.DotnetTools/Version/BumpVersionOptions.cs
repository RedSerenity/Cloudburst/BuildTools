using System;
using CommandLine;

namespace Cloudburst.BuildTools.DotnetTools.Version {

	[Verb("bump-version", HelpText = "Bump the version number")]
	public class BumpVersionOptions {
		private VersionType _versionType;
		private string _versionFile;

		public VersionType RawVersionType => _versionType;

		[Option('f', "field", Required = true, HelpText = "Major, Minor, Patch, Build")]
		public string VersionType {
			get => _versionType.ToString();
			set {
				try {
					_versionType = Enum.Parse<VersionType>(value);
				} catch (Exception) {
					throw new Exception("Invalid option for --bump. Must be Major, Minor, Patch, or Build");
				}
			}
		}

		[Option('e', "versionFile", Required = false, HelpText = "Version file to modify. Defaults to 'Version'")]
		public string VersionFile {
			get => String.IsNullOrEmpty(_versionFile) ? "Version" : _versionFile;
			set => _versionFile = value;
		}
	}
}
