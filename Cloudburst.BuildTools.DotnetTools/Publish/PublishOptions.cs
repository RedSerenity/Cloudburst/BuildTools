using CommandLine;

namespace Cloudburst.BuildTools.DotnetTools.Publish {
	[Verb("publish", HelpText = "Publish a package to Nuget.org")]
	public class PublishOptions {
		[Option('p', "package", Required = true, HelpText = "Full path to package.")]
		public string Package { get; set; }

		[Option('k', "key", Required = false, HelpText = "Api Key for Nuget.org")]
		public string ApiKey { get; set; }

		[Option('i', "ignoreExisting", Required = false, HelpText = "Ignore existing published package.")]
		public bool IgnoreExisting { get; set; }
	}
}
