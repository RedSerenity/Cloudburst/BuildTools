using System.Collections.Generic;
using NuGet.Versioning;

namespace Cloudburst.BuildTools.DotnetTools.Publish {
	public class NuGetPackage {
		public string Id { get; set; }
		public NuGetVersion LatestVersion { get; set; }
		public IEnumerable<NuGetVersion> AllVersions { get; set; }
		public string RepositorySource { get; set; }
	}
}
