using System.Collections.Generic;
using System.IO;
using System.Linq;
using NuGet.Configuration;

namespace Cloudburst.BuildTools.DotnetTools.Publish {
	public class NuGetConfiguration {
		public ISettings Settings { get; }
		public IPackageSourceProvider PackageSourceProvider { get; }
		public IList<PackageSource> PackageSources { get; }

		public NuGetConfiguration() {
			string currentDirectory = Directory.GetCurrentDirectory();
			Settings = new Settings(currentDirectory);

			PackageSourceProvider = new PackageSourceProvider(Settings);
			PackageSources = PackageSourceProvider
				.LoadPackageSources()
				.Where(x => x.IsEnabled)
				.ToList();
		}
	}
}
