using System;
using System.IO;
using System.Threading.Tasks;

namespace Cloudburst.BuildTools.DotnetTools.Publish {
	public class PublishCommand {
		public static async Task<int> RunPublishAsync(PublishOptions options) {
			var configuration = new NuGetConfiguration();
			var packages = new NuGetPackagesRepository(configuration);

			var path = Path.GetDirectoryName(options.Package);
			var fileSearch = Path.GetFileName(options.Package);
			string[] files = Directory.GetFiles(path, fileSearch, SearchOption.TopDirectoryOnly);

			if (files.Length == 0) {
				Console.WriteLine($"Error: No packages found at: {options.Package}");
				return 1;
			}

			foreach (string file in files) {
				if (!File.Exists(file)) {
					Console.WriteLine($"Error: Specified package does not exist at path: {file}");
					return 1;
				}

				await packages.PushAsync(file, options.ApiKey, options.IgnoreExisting);
			}

			return 0;
		}
	}
}
