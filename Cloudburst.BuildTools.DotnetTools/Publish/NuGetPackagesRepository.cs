using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using NuGet.Commands;
using NuGet.Common;
using NuGet.Configuration;
using NuGet.Protocol;
using NuGet.Protocol.Core.Types;

namespace Cloudburst.BuildTools.DotnetTools.Publish {
	public class NuGetPackagesRepository {
		private NuGetConfiguration Configuration { get; }
		public int PushTimeout { get; set; } = 30;

		public NuGetPackagesRepository(NuGetConfiguration configuration) {
			Configuration = configuration;
		}

		public async Task<IList<NuGetPackage>> ListAsync(string searchTerm, bool includeAllVersions = true, bool includePreRelease = true, bool includeDelisted = false) {
			if (searchTerm is null) {
				throw new ArgumentNullException(nameof(searchTerm));
			}

			IList<NuGetPackage> packagesList = new List<NuGetPackage>();

			foreach (PackageSource packageSource in Configuration.PackageSources) {
				SourceRepository sourceRepository = Repository.Factory.GetCoreV3(packageSource.Source);
				ListResource feed = await sourceRepository.GetResourceAsync<ListResource>();
				if (feed is null) continue;

				var packages = await feed.ListAsync(searchTerm, includePreRelease, includeAllVersions, includeDelisted, NullLogger.Instance, CancellationToken.None);
				if (packages is null) continue;

				var packagesEnumerator = packages.GetEnumeratorAsync();
				while (await packagesEnumerator.MoveNextAsync()) {
					IPackageSearchMetadata package = packagesEnumerator.Current;
					var packageVersions = await package.GetVersionsAsync();

					packagesList.Add(new NuGetPackage {
						Id = package.Identity.Id,
						LatestVersion = package.Identity.Version,
						AllVersions = packageVersions.Select(x => x.Version).ToList()
					});
				}
			}

			return packagesList;
		}

		public async Task PushAsync(string packagePath, string apiKey, bool ignoreExisting = true) {
			await PushRunner.Run(
				Configuration.Settings,
				Configuration.PackageSourceProvider,
				packagePath,
				Configuration.PackageSources.First().Source,
				apiKey,
				null,
				null,
				PushTimeout,
				true,
				true,
				true,
				ignoreExisting,
				NullLogger.Instance
			);
		}

	}
}
