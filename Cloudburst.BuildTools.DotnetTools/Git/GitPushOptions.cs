using CommandLine;

namespace Cloudburst.BuildTools.DotnetTools.Git {
	[Verb("push-all", HelpText = "Push all git repos upstream, including children")]
	public class GitPushOptions {

	}
}


/*
public static bool createTag(string tag,string localRepoPath)
    {
        var repo = new Repository(localRepoPath);
        if (repo == null)
        {
            Console.WriteLine(DateTime.Now + "No repository exists in " + localRepoPath);
            return false;
        }
        Tag t = repo.ApplyTag(tag);
        if (t == null)
        {
            Console.WriteLine(DateTime.Now + "Could not create tag :" + tag);
            return false;
        }
        else
            Console.WriteLine(DateTime.Now + "Tag has been created successfully :" + tag);
        return true;
    }

    //push the tags
    public static bool pushTags(string tag, string localRepoPath)
    {
        try
        {
            Credentials creds = new UsernamePasswordCredentials()
            {
                Username = USERNAME,
                Password = PASSWORD
            };
            CredentialsHandler ccd = (url, usernameFromUrl, types) => creds;
            PushOptions options = new PushOptions { CredentialsProvider = ccd };
            string rfspec = "refs/tags/" + tag;
            using (Repository repo = new Repository(localRepoPath))
            {
                Remote remote = repo.Network.Remotes["origin"];
                repo.Network.Push(remote, rfspec, rfspec, options);

            }
        }
        catch(Exception ex)
        {
            Console.WriteLine(DateTime.Now + "----#Errors in Push tag " + tag + " " + ex.Message);
            return false;
        }

        return true;
    }
    */
