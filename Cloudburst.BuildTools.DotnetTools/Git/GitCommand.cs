using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using LibGit2Sharp;

namespace Cloudburst.BuildTools.DotnetTools.Git {
	public class GitCommand {

		public static async Task<int> RunGitCommitAsync(GitCommitOptions options) {
			await CommitRepository(Directory.GetCurrentDirectory(), options);

			var childDirectories = Directory.EnumerateDirectories(Directory.GetCurrentDirectory());

			foreach (string directory in childDirectories) {
				await CommitRepository(directory, options);
			}

			return 0;
		}

		private static async Task CommitRepository(string directory, GitCommitOptions options) {
			try {
				using (Repository repo = new Repository(directory)) {
					Signature author;

					if (!String.IsNullOrEmpty(options.Author) || !String.IsNullOrEmpty(options.Email)) {
						author = new Signature(options.Author, options.Email, DateTimeOffset.Now);
					} else {
						author = repo.Config.BuildSignature(DateTimeOffset.Now);
					}

					RepositoryStatus status = repo.RetrieveStatus();
					IEnumerable<string> filePaths = status.Modified.Select(mods => mods.FilePath);
					Commands.Stage(repo, filePaths);

					repo.Commit(options.Message, author, author);

					if (options.TagCommitWithVersion) {
						string version = await File.ReadAllTextAsync(options.VersionFile);
						string splitPattern = @"\.";
						Regex regex = new Regex(splitPattern);
						string[] splitVersion = regex.Split(version);

						if (String.Equals(repo.Head.FriendlyName, "master")) {
							repo.ApplyTag($"v{String.Join('.', splitPattern[0], splitPattern[1], splitPattern[2])}");
						}

						if (String.Equals(repo.Head.FriendlyName, "develop")) {
							repo.ApplyTag($"v{String.Join('.', splitPattern)}");
						}
					}
				}
			} catch (RepositoryNotFoundException exception) {
				Console.WriteLine($"Directory {directory} is not a git repository.");
			}
		}

		public static async Task<int> RunGitPushAsync(GitPushOptions options) {
			await PushRepositoryUpstream(Directory.GetCurrentDirectory(), options);

			var childDirectories = Directory.EnumerateDirectories(Directory.GetCurrentDirectory());

			foreach (string directory in childDirectories) {
				await PushRepositoryUpstream(directory, options);
			}


			return 0;
		}

		private static async Task PushRepositoryUpstream(string directory, GitPushOptions options) {
			try {
				using (Repository repo = new Repository(directory)) {
					Branch currentBranch = repo.Branches[repo.Head.FriendlyName];
					repo.Network.Push(currentBranch);
				}
			} catch (RepositoryNotFoundException exception) {
				Console.WriteLine($"Directory {directory} is not a git repository.");
			}
		}

	}
}
