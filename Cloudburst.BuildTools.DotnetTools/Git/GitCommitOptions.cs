using System;
using CommandLine;

namespace Cloudburst.BuildTools.DotnetTools.Git {
	[Verb("commit-all", HelpText = "Commit all git repos, including children")]
	public class GitCommitOptions {
		private string _versionFile;

		[Option('m', "message", Required = true, HelpText = "The common commit message to apply")]
		public string Message { get; set; }

		[Option('t', "tag", Required = false, HelpText = "Use the version file to tag the commit")]
		public bool TagCommitWithVersion { get; set; }

		[Option('e', "versionFile", Required = false, HelpText = "Version file to use when tagging the commit. Defaults to 'Version'")]
		public string VersionFile {
			get => String.IsNullOrEmpty(_versionFile) ? "Version" : _versionFile;
			set => _versionFile = value;
		}

		[Option('a', "author", Required = false, HelpText = "Set commit author")]
		public string Author { get; set; }

		[Option('e', "email", Required = false, HelpText = "Set commit email")]
		public string Email { get; set; }
	}
}
