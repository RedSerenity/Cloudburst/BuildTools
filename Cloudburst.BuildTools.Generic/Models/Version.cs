namespace Cloudburst.BuildTools.Generic.Models {
	public class Version {
		public int Major { get; set; } = 0;
		public int Minor { get; set; } = 1;
		public int Patch { get; set; } = 0;
		public int Build { get; set; } = 1000;
	}
}
