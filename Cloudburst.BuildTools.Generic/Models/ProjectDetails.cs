using System;
using Microsoft.Build.Framework;

namespace Cloudburst.BuildTools.Generic.Models {
	public class ProjectDetails {
		[Required]
		public string Namespace { get; set; }

		[Required]
		public string Title { get; set; }
		[Required]
		public string Description { get; set; }

		[Required]
		public string RepositoryName { get; set; }
		public string RepositoryUrl { get; set; }
		public string RepositoryBaseRawUrl { get; set; }
		public string RepositoryBaseAddress { get; set; }

		public string ProjectUrl { get; set; }
		public string PackageProjectUrl { get; set; }
		public string ProjectLicenseType { get; set; } = "MIT";
		public string ProjectLicenseFile { get; set; } = "LICENSE";
		public string ProjectIconFile { get; set; } = "icon.png";
		public string ProjectAuthor { get; set; }
		public string ProjectCopyright => $"Copyright © {DateTime.Now.Year} {ProjectAuthor}";

		public Version Version { get; set; } = new Version();
	}
}
