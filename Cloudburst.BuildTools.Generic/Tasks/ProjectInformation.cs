using System;
using System.IO;
using System.Reflection;
using System.Text.Json;
using Cloudburst.BuildTools.Generic.Models;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using Version = Cloudburst.BuildTools.Generic.Models.Version;

namespace Cloudburst.BuildTools.Generic.Tasks {
	public class ProjectInformation : Task {
		[Required]
		public string JsonFile { get; set; }

		[Output]
		public ITaskItem Details { get; set; }
		[Output]
		public ITaskItem Version { get; set; }

		//https://github.com/dotnet/runtime/issues/31433

		public override bool Execute() {
			if (String.IsNullOrEmpty(JsonFile)) {
				throw new ArgumentNullException(nameof(JsonFile));
			}

			string currentDirectory = Directory.GetCurrentDirectory();
			string fullPath = Path.GetFullPath($"{currentDirectory}/{JsonFile}");

			if (!File.Exists(fullPath)) {
				Log.LogWarning($"File '{fullPath}' does not exist. Moving one directory up and trying again.");
				// If it doesn't exist, let's try the parent directory before giving up.
				DirectoryInfo parentPath = Directory.GetParent(currentDirectory);
				fullPath = Path.GetFullPath($"{parentPath}/{JsonFile}");

				if (!File.Exists(fullPath)) {
					Log.LogError($"File '{fullPath}' does not exist!");
					Log.LogErrorFromException(new FileNotFoundException("Could not find file.", JsonFile));
					return false;
				}
			}

			Log.LogMessage($"Loading {JsonFile} from {fullPath}");

			try {
				string projectJson = File.ReadAllText(fullPath);
				Log.LogMessage(projectJson);
				var project = JsonSerializer.Deserialize<ProjectDetails>(projectJson, new JsonSerializerOptions {
					PropertyNameCaseInsensitive = true
				});

				Log.LogMessage($"Incrementing build version from {project.Version.Build} to {++project.Version.Build}.");

				project.Version.Build++;

				var projectSerialized = JsonSerializer.Serialize(project, new JsonSerializerOptions {
					WriteIndented = true
				});

				File.WriteAllText(fullPath, projectSerialized);

				ITaskItem details = new TaskItem(project.Namespace);
				ITaskItem version = new TaskItem($"{project.Namespace} Version");

				foreach (PropertyInfo property in project.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance)) {
					if (property.PropertyType == typeof(Version)) {
						Version theVersion = (Version) property.GetValue(project);
						PropertyInfo[] versionProperties = theVersion.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
						foreach (PropertyInfo versionProperty in versionProperties) {
							var versionValue = (int) versionProperty.GetValue(theVersion);
							version.SetMetadata(versionProperty.Name, versionValue.ToString());
						}

						continue;
					}

					details.SetMetadata(property.Name, (string) property.GetValue(project));
				}

				Details = details;
				Version = version;
			} catch (Exception exception) {
				Log.LogError(exception.Message);
				return false;
			}

			return true;
		}
	}

}
